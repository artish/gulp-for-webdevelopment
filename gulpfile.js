/*-------------------------------------------------------*\
* Require
\*-------------------------------------------------------*/

var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')({ camelize: true }),
    gutil = require('gulp-util'),
    shell = require('gulp-shell'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync');

/*-------------------------------------------------------*\
* Variables
\*-------------------------------------------------------*/

var app            = '../app',
    src            = '../src',
    build          = '../build',
    assets         = (app         + '/assets'),
    img_src_dir    = (src         + '/img'),
    img_dst_dir    = (assets      + '/img'),
    scss_src_dir   = (src         + '/scss'),
    scss_dst_dir   = (assets      + '/css'),
    js_src_dir     = (src         + '/js'),
    js_dst_dir     = (assets      + '/js'),
    svg_src_dir    = (src         + '/svg'),
    svg_dst_dir    = (assets      + '/svg'),
    js_hint_dev    = (js_src_dir  + '/.jshintrc'),
    js_src_lib     = (js_src_dir  + '/lib'),
    js_dst_lib     = (js_dst_dir  + '/lib'),
    sprite_src_dir = (src         + '/sprites'),
    sprite_dst_dir = (img_dst_dir + '/sprites')
    ;

/*-------------------------------------------------------*\
* Testing
\*-------------------------------------------------------*/

// List of all plugins that are got from the gulp-plugins module
gulp.task('echo-plugins', function() {
    gutil.log(plugins, gutil.colors.cyan('123'));
});

// If you have problems with the caching
// call this function to clear the cache
gulp.task('clear-cache', function() {
    plugins.cache.clearAll();
});

/*-------------------------------------------------------*\
* Browser Sync Task
\*-------------------------------------------------------*/

// Gulp Browser Sync
// Look in the README for more information and how to set it up
// [BrowserSync + Gulp.js](http://www.browsersync.io/docs/gulp/)
gulp.task('browser-sync', function() {
    browserSync.init(scss_dst_dir + '*.css', {
        // http://www.browsersync.io/docs/options/#option-ghostMode
        ghostMode: {
            clicks: true,
            location: true,
            forms: true,
            scroll: false
        },
        // Will not attempt to determine your network status, assumes you're ONLINE.
        // http://www.browsersync.io/docs/options/#option-online
        online: true,
        scrollThrottle: 10,
        reloadDelay: 100,
     });
});

/*-------------------------------------------------------*\
* Style Tasks
\*-------------------------------------------------------*/

var options = {};
options.sass = {
    errLogToConsole: true,
    // sourceComments: 'map',
    // sourceMap: 'scss'
}

// Styles for development
// libSASS & Autoprefixer
// [gulp-sass](https://www.npmjs.org/package/gulp-sass)
// [gulp-autoprefixer](https://www.npmjs.org/package/gulp-autoprefixer)
gulp.task('styles-dev', function() {
  return gulp.src(scss_src_dir + '/style.scss')
    .pipe(plugins.sass(options.sass))
    .pipe(plugins.autoprefixer('last 2 versions', 'ie 9', 'ios 6', 'android 4'))
    .pipe(gulp.dest(scss_dst_dir))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('styles-build', function() {
  return gulp.src(scss_src_dir + '/style.scss')
    .pipe(plugins.sass({ errLogToConsole: true, style: 'compressed' }))
    .pipe(plugins.autoprefixer('last 2 versions', 'ie 9', 'ios 6', 'android 4'))
    .pipe(plugins.minifyCss())
    .pipe(gulp.dest(scss_dst_dir));
});

/*-------------------------------------------------------*\
* Script Tasks
\*-------------------------------------------------------*/
 
// Javascript for development
// JSHint and Concat the javascript files
gulp.task('scripts-dev', function() {
  return gulp.src([(js_src_dir + '/*.js'), ('!' + js_src_dir + '/plugins.js'), ('!' + js_src_dir + '/wip')])
    .pipe(plugins.jshint(js_hint_dev))
    .pipe(plugins.jshint.reporter('default'))
    .pipe(plugins.concat('script.js'))
    .pipe(gulp.dest(js_dst_dir));
});

// Javascript for production
// Concat & Uglify the the javascript files
gulp.task('scripts-build', function() {
  return gulp.src([(js_src_dir + '/*.js'), ('!' + js_src_dir + '/plugins.js'), ('!' + js_src_dir + '/wip')])
    // .pipe(plugins.jshint(js_hint_dev))
    // .pipe(plugins.jshint.reporter('default'))
    .pipe(plugins.concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest(js_dst_dir));
});

// JS Libs Task
// Copy the libraries from the source directory to the assets folder
gulp.task('copy-script-libs', function(){
    return gulp.src([js_src_lib + '/*.js'])
    .pipe(gulp.dest(js_dst_lib));
});

/*-------------------------------------------------------*\
* Sprite Task
\*-------------------------------------------------------*/

// Create spritesheet image
// Needs glue installed
gulp.task('glue-dev', function() { 
    return gulp.src('', {read:false})
    .pipe(shell([
        'glue \
            ../src/sprites \
            ../src/sprites/final \
            --url=../img/sprites/ \
            --scss-template=glue.jinja  \
            --scss=../src/scss/sprites \
            --no-css'
    ]))
})

// Sprite image optimization
gulp.task('sprites-img', function() {
  return gulp.src((sprite_src_dir + '/final/*'))
    .pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 7, progressive: true, interlaced: true })))
    .pipe(gulp.dest(sprite_dst_dir));
});


/*-------------------------------------------------------*\
* Image Task
\*-------------------------------------------------------*/


// Images
// The image optimization is cached by gulp so if you run into errors try deleting the cache
// 
gulp.task('images', function() {
  return gulp.src((img_src_dir + '/**/**/*'))
    .pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 7, progressive: true, interlaced: true })))
    .pipe(gulp.dest(img_dst_dir));
});

// SVG Minimize
// [gulp-svgmin](https://www.npmjs.org/package/gulp-svgmin)
gulp.task('svg-min', function() {
    return gulp.src(svg_src_dir + '/*.svg')
        .pipe(plugins.svgmin())
        .pipe(gulp.dest(svg_dst_dir));
});


/*-------------------------------------------------------*\
* Template Tasks
\*-------------------------------------------------------*/

gulp.task('bs-reload', function () {
    browserSync.reload();
});

/*-------------------------------------------------------*\
* Watch Tasks
\*-------------------------------------------------------*/

gulp.task('watch', function() {
 
    // SASS
    gulp.watch((scss_src_dir + '/**/**/*.scss'), ['styles-dev']);

    // PHP
    gulp.watch((app + '/**/**/*.php'), ['bs-reload']);
    gulp.watch((app + '/**/**/*.html'), ['bs-reload']);

    // HTML
    // gulp.watch((app + '/*.html'), ['bs-reload']);

    // SVG
    gulp.watch((svg_src_dir + '/*.svg'), ['svg-min', 'bs-reload']);

    // Javascript
    gulp.watch((js_src_dir+ '/*.js'), ['scripts-dev', 'bs-reload']);

    // Watch image Files
    gulp.watch((img_src_dir + '/**/*.*'), ['images', 'bs-reload']);
 
    // Wath sprite Files
    gulp.watch((sprite_src_dir + '/*'), ['glue-dev', 'sprites-img', 'bs-reload']);

});

/*-------------------------------------------------------*\
* Deploy tasks
\*-------------------------------------------------------*/

gulp.task('build-push', plugins.shell.task([
    'rsync -av ../app/ ../build/',
    'rm -rf ../build/content/01-portfolio/**/assets',
    'rm -rf ../build/site/snippets/development',
    'cd ../build; git add -A .',
    'cd ../build; git commit -am "[Deploy] - $(date "+%Y %m %d - %T") - $(whoami)"',
    'cd ../build; git push origin master'
]))

/*-------------------------------------------------------*\
* Main Tasks
\*-------------------------------------------------------*/

// Default task to be run with gulp
gulp.task('default', ['copy-script-libs', 'styles-dev', 'scripts-dev', 'svg-min', 'images', 'browser-sync', 'watch']);

gulp.task('build', ['copy-script-libs', 'scripts-build', 'styles-build', 'scripts-dev', 'svg-min', 'build-push']);

