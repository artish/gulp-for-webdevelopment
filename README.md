Gulp for WebDevelopment
=======================

## Complete Removal of Node & NPM

  go to /usr/local/lib and delete any node and node_modules    
  go to /usr/local/include and delete any node and node_modules directory    
  if you installed with brew install node, then run brew uninstall node in your terminal    
  check your Home directory for any "local" or "lib" or "include" folders, and delete any "node" or "node_modules" from there    
  go to /usr/local/bin and delete any node executable    
  You may need to do the additional instructions as well:    
      
  remove: /usr/local/bin/npm    
  remove: /usr/local/share/man/man1/node.1    
  remove: /usr/local/lib/dtrace/node.d    
  execute: rm -rf /Users/[homedir]/.npm    
  Then download nvm and follow the instructions to install node. The latest versions of node come with npm, I believe, but you can also reinstall that as well.    

  [Stackoverflow Reference](http://stackoverflow.com/questions/11177954/how-do-i-completely-uninstall-node-js-and-reinstall-from-beginning-mac-os-x)

## Installation

  To set up gulp install Node using [Homebrew](http://brew.sh/)

    #!/bin/sh
    brew install node

  1. Then install gulp    
  `npm install -g gulp`

  2. Chown the npm & node-gyp directory
  `sudo chown -R $(whoami) ~/.npm`    
  `sudo chown -R $(whoami) ~/.node-gyp`
  `sudo chown -R $(whoami) /usr/local/lib/node_modules`

  3. Finall install the node packages
  `npm install`

  4. Install python    
     Python is needed for the [glue CLT](https://github.com/jorgebastida/glue)    
     `brew install python`

  5. Install Glue    
     `pip install glue`

  6. Install OptiPNG to optimize the PNGs
     `brew install optipng`


## Description

  Gulp for webdevelopment, install as a submodule.

* * *

## BrowserSync & MAMP Setup

  Using this setup, mamp will work with browser sync.     
  This way you can view your work on any browser in the given network.

### Mamp Setup

  Insert the IP using 

  `$ ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}'`

  Set up MAMP using the following settings:  

  ![](http://i.imgur.com/z9gbnZs.png)

  ![](http://i.imgur.com/e8yaCtg.png)
  
  At last insert der javascript connect into the scripts body.